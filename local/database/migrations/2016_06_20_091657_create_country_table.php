<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCountryTable
 *
 * php artisan make:migration create_country_table
 *
 * php artisan migrate
 *
 * composer require --dev --no-update "xethron/migrations-generator:dev-l5"
composer require --dev --no-update "way/generators:dev-feature/laravel-five-stable"
composer require --dev --no-update "user11001/eloquent-model-generator:~2.0"
composer config repositories.repo-name git "git@github.com:jamisonvalenta/Laravel-4-Generators.git"
composer update
 *
 * (Providers)
 * Way\Generators\GeneratorsServiceProvider::class,
Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class,
User11001\EloquentModelGenerator\EloquentModelGeneratorProvider::class,
 *
 * php artisan migrate:generate
 * 
 * php artisan models:generate --path="app/Models/Orm" --namespace="App/Models/Orm"
 *
 */

class CreateCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullName', 45)->index();
            $table->string('shortName', 10);
            $table->integer('population')->comment('人口數');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('country');
    }
}
