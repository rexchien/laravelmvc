<?php
/**
 * Created by PhpStorm.
 * User: rex.chien
 * Date: 2016/6/20
 * Time: 下午6:11
 */

namespace App\Http\Controllers;


use App\Http\Requests\CountryFormRequest;
use App\Models\Orm\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function index()
    {
        return view('country.index', [
            'countries' => Country::all()
        ]);
    }

    public function insertView()
    {
        return view('country.form', [
            'country' => new Country()
        ]);
    }

    public function insertAction(Country $country, CountryFormRequest $request)
    {
        $country->create($request->all());

        return redirect('country');
    }

    public function updateView(Country $country)
    {
        return view('country.form', [
            'country' => $country
        ]);
    }

    public function updateAction(Country $country, CountryFormRequest $request)
    {
        $country->update($request->all());

        return redirect('country');
    }

    public function deleteAction(Country $country)
    {
        $country->delete();

        return redirect('country');
    }
}