<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class LocaleResolverMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $locale = $request->input('lang');

        if (!$locale) {
            $locale = $request->cookie('locale', config('app.fallback_locale'));
        }

        App::setLocale($locale);

        $response = $next($request);

        if ($request->has('lang')) {
            $response->withCookie(cookie()->forever('locale', $locale));
        }

        return $response;
    }
}
