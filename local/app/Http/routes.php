<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => '/country'], function () {
    Route::get('', 'CountryController@index');
    Route::get('/insert', 'CountryController@insertView');
    Route::post('/insert', 'CountryController@insertAction');
    Route::get('/{country}/update', 'CountryController@updateView');
    Route::post('/{country}/update', 'CountryController@updateAction');
    Route::get('/{country}/delete', 'CountryController@deleteAction');
});