<?php namespace App\Models\Orm;

use Illuminate\Database\Eloquent\Model;

class Migration extends Model {

    /**
     * Generated
     */

    protected $table = 'migrations';
    protected $fillable = ['migration', 'batch'];



}
