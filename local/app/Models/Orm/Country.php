<?php namespace App\Models\Orm;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

    /**
     * Generated
     */

    protected $table = 'country';
    protected $fillable = ['id', 'fullName', 'shortName', 'population'];



}
