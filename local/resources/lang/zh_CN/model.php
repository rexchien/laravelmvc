<?php
/**
 * Created by PhpStorm.
 * User: rex.chien
 * Date: 2016/6/23
 * Time: 上午10:36
 */


return [
    'country' => [
        'fullName' => '全名',
        'shortName' => '缩写',
        'population' => '人口'
    ]
];