<?php
/**
 * Created by PhpStorm.
 * User: rex.chien
 * Date: 2016/6/23
 * Time: 上午10:36
 */


return [
    'country' => [
        'fullName' => 'Full Name',
        'shortName' => 'Short Name',
        'population' => 'Population'
    ]
];