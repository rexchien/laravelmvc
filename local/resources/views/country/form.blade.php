@extends('layout.default')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{ Form::model($country) }}
    <div class="form-group">
        {{ Form::label('fullName', trans('model.country.fullName')) }}
        {{ Form::text('fullName', $country->fullName, ['size' => 45, 'class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('shortName', trans('model.country.shortName')) }}
        {{ Form::text('shortName', $country->shortName, ['size' => 10, 'class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('population', trans('model.country.population')) }}
        {{ Form::number('population', $country->population, ['class' => 'form-control']) }}
    </div>
    {{ Form::submit(trans('function.submit'), ['class' => 'btn btn-primary']) }}
    {{ Form::close() }}
@endsection