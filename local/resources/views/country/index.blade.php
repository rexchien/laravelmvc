@extends('layout.default')

@section('content')
    <div class="page-header">
        <h1>首頁</h1>
        <a class="btn btn-default" href="{{ URL::to('/country/insert') }}">
            {{ trans('function.insert') }}
        </a>
    </div>

    <table class="table">
        <tr>
            <th>全名</th>
            <th>縮寫</th>
            <th>人口</th>
            <th></th>
        </tr>

        @foreach($countries as $country)
            <tr>
                <td>{{ $country->fullName }}</td>
                <td>{{ $country->shortName }}</td>
                <td>{{ $country->population }}</td>
                <td>
                    <a href="{{ URL::to("/country/{$country->id}/update") }}">
                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    </a>
                    <a href="{{ URL::to("/country/{$country->id}/delete") }}">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </a>
                </td>
            </tr>
        @endforeach
    </table>
@endsection