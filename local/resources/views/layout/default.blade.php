<!DOCTYPE html>
<html >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel MVC Sample</title>

    {{ Html::style('css/bootstrap.min.css') }}
    {{ Html::style('css/font-awesome.min.css') }}
    {{ Html::style('css/style.css') }}

    {{ Html::script('js/jquery-2.2.4.min.js') }}
</head>
<body>
@include('layout.header')
<div class="container content">
    @yield('content')
</div>
@include('layout.footer')
</body>

{{ Html::script('js/bootstrap.min.js') }}

</html>