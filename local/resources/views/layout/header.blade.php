<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('/') }}">Laravel MVC Sample</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="?lang=zh_TW">繁中</a></li>
                <li><a href="?lang=zh_CN">简中</a></li>
                <li><a href="?lang=en_US">ENG</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
