<?php $__env->startSection('content'); ?>
    <?php if(count($errors) > 0): ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach($errors->all() as $error): ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?php echo e(Form::model($country)); ?>

    <div class="form-group">
        <?php echo e(Form::label('fullName', trans('model.country.fullName'))); ?>

        <?php echo e(Form::text('fullName', $country->fullName, ['size' => 45, 'class' => 'form-control'])); ?>

    </div>
    <div class="form-group">
        <?php echo e(Form::label('shortName', trans('model.country.shortName'))); ?>

        <?php echo e(Form::text('shortName', $country->shortName, ['size' => 10, 'class' => 'form-control'])); ?>

    </div>
    <div class="form-group">
        <?php echo e(Form::label('population', trans('model.country.population'))); ?>

        <?php echo e(Form::number('population', $country->population, ['class' => 'form-control'])); ?>

    </div>
    <?php echo e(Form::submit(trans('function.submit'), ['class' => 'btn btn-primary'])); ?>

    <?php echo e(Form::close()); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>