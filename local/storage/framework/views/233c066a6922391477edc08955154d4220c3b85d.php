<!DOCTYPE html>
<html >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel MVC Sample</title>

    <?php echo e(Html::style('css/bootstrap.min.css')); ?>

    <?php echo e(Html::style('css/font-awesome.min.css')); ?>

    <?php echo e(Html::style('css/style.css')); ?>


    <?php echo e(Html::script('js/jquery-2.2.4.min.js')); ?>

</head>
<body>
<?php echo $__env->make('layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="container content">
    <?php echo $__env->yieldContent('content'); ?>
</div>
<?php echo $__env->make('layout.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>

<?php echo e(Html::script('js/bootstrap.min.js')); ?>


</html>